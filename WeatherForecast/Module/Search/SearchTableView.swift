//
//  SearchTableView.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation
import UIKit

class SearchTableView:UITableView{
    
    var searchedCityElements:[SearchCityElement]
    
    init(searchedCityElements: [SearchCityElement]) {
        self.searchedCityElements = searchedCityElements
        setView()
    }
    
    func setView(){
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
