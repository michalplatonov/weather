//
//  SearchTableViewCell.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import UIKit
import LBTATools
import SnapKit

class SearchTableViewCell:UITableViewCell{
    var searchCityElement:SearchCityElement?{
        didSet{
            guard let searchCityElement = searchCityElement else {
                return
            }
            searchedCityNameLabel.text = searchCityElement.localizedName
            searchedCountryNameLabel.text = searchCityElement.country.localizedName
            searchedCountryCityImageView.image = UIImage(named: searchCityElement.country.id.lowercased())
        }
    }
    let searchedCountryCityImageView = UIImageView(image: UIImage(data: Data()))
    let searchedCityNameLabel = UILabel(text: "Nie znany", font: .boldFont(with: .middle), textColor: .textColor, textAlignment: .left, numberOfLines: 0)
    let searchedCountryNameLabel = UILabel(text: "Nie znany", font: .regularFont(with: .small), textColor: .textColor, textAlignment: .left, numberOfLines: 0)
    let view = UIView(backgroundColor: .lightGray.withAlphaComponent(0.3))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
        selectionStyle = .none
        backgroundColor = .clear
    }
    func setView(){
        let stackView = stack(searchedCityNameLabel,searchedCountryNameLabel,spacing: 0,alignment: .leading,distribution: .fillEqually)
        contentView.addSubview(view)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.snp.makeConstraints { make in
            make.top.equalTo(contentView.snp.top).offset(5)
            make.bottom.equalTo(contentView.snp.bottom).offset(-5)
            make.left.equalTo(contentView.snp.left).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
        }
        [searchedCountryCityImageView,stackView].forEach{view.addSubview($0)}
        searchedCountryCityImageView.layer.cornerRadius = 10
        searchedCountryCityImageView.layer.masksToBounds = true
        searchedCountryCityImageView.snp.makeConstraints { make in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(view.snp.left).offset(5)
            make.height.equalTo(40)
            make.width.equalTo(40)
        }
        stackView.snp.makeConstraints { make in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(searchedCountryCityImageView.snp.right).offset(15)
            make.right.equalTo(view.snp.right).offset(-5)
            make.height.equalTo(40)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
