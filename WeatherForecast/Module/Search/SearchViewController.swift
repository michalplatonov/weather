//
//  ViewController.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import UIKit
import SnapKit

class SearchViewController: BaseViewController {

    private let citySearchBar = CitySearchBar()
    private let searchedcityDataSource = SearchedCityDataSource()
    private let tableView:UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorColor = .clear
        tableView.separatorInset = .zero
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        return tableView
    }()
    private var searchedCityList:[SearchCityElement]?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Weather Forecast"
        setView()
        setTableView()
    }
}

//MARK: UI
extension SearchViewController{
    func setView(){
        [citySearchBar,tableView].forEach{view.addSubview($0)}
        citySearchBar.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(10)
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left).offset(10)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right).offset(-10)
            make.height.equalTo(50)
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(citySearchBar.snp.bottom).offset(10)
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
        citySearchBar.citySearchBarDelegate = self
    }
    func setTableView(){
        tableView.register(SearchTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = searchedcityDataSource
        tableView.delegate = self
    }
}

extension SearchViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let searchedCityList = searchedCityList else {return}
        let searchCityElement = searchedCityList[indexPath.row]
        let forecastViewController = ForecastViewController(searchCityElement: searchCityElement)
        self.navigationController?.pushViewController(forecastViewController, animated: true)
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let searchedCityList = searchedCityList else {return UIView()}
        let headerView = UIView(backgroundColor: .clear)
        let labelText = searchedCityList.count > 0 ? "Znaleziono: \(searchedCityList.count)" : "Nic nie znaleziono"
        let headerViewLabel = UILabel(text: labelText, font: .regularFont(with: .middle), textColor: .textColor, textAlignment: .left, numberOfLines: 1)
        headerView.addSubview(headerViewLabel)
        headerViewLabel.snp.makeConstraints { make in
            make.centerY.equalTo(headerView.snp.centerY)
            make.left.equalTo(headerView.snp.left).offset(10)
        }
        return headerView
    }
}

extension SearchViewController:CitySearchBarDelegate{
    func getSearchedText(_ text: String?) {
        if let text = text {
            getSearchedCityList(where: text)
        }
    }
}

//MARK: get list of searched cities
extension SearchViewController{
    func getSearchedCityList(where searchedText:String){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let queryItem = URLQueryItem(name: "q", value: searchedText)
        let endpoint = EndPoint(path: Path.locationCitySearch.rawValue, queryItems: [queryItem])
        let service = SearchCityRequest<SearchCityElement>(endPoint: endpoint)
        service.getSearchedCityList { [weak self] result in
            switch result{
            case .success(let searchedCityList):
                self?.searchedCityList = searchedCityList
                DispatchQueue.main.async {
                    self?.searchedcityDataSource.data = searchedCityList
                    self?.tableView.reloadData()
                }
            case .failure(let error):
                guard let error = error as? ApiError,let message = error.message else {
                    self?.showAlertViewController(where: error.localizedDescription)
                    return
                }
                self?.showAlertViewController(where: message)
                
            }
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
}
