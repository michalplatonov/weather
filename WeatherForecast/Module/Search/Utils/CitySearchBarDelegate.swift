//
//  CitySearchBarDelegate.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation

protocol CitySearchBarDelegate:AnyObject{
    func getSearchedText(_ text:String?)
}
