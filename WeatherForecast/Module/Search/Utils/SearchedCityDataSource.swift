//
//  SearchedCityDataSource.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation
import UIKit

class SearchedCityDataSource:NSObject,UITableViewDataSource{
    var data:[SearchCityElement]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let data = data,data.count > 0 else {
            tableView.setEmptyTitle("Brak wyników")
            return 0
        }
        tableView.restore()
        return data.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = data,let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SearchTableViewCell else {return UITableViewCell()}
        let searchCityElement = data[indexPath.row]
        cell.searchCityElement = searchCityElement
        return cell
        
        
    }
}
