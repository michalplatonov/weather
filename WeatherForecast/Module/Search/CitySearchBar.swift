//
//  CitySearchBar.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation
import UIKit


class CitySearchBar:UISearchBar{
    
    weak var citySearchBarDelegate:CitySearchBarDelegate?
    var searchText:String?
    override init(frame: CGRect) {
        super.init(frame: frame)
        setSearchBar()
        customizeSearchBar()
    }
    private func setSearchBar(){
        searchBarStyle = .default
        placeholder = "Szukaj lokalizacji"
        setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        backgroundColor = .white
        barTintColor = .white
        tintColor = .textColor
        layer.borderColor = UIColor.textColor.cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 10
        layer.masksToBounds = true
        returnKeyType = .done
        delegate = self
    }
    private func customizeSearchBar(){
        guard let searchField = value(forKey: "searchField") as? UITextField,let placeholderLabel = searchField.value(forKey: "placeholderLabel") as? UILabel else {return}
        searchField.leftView?.tintColor = .textColor
        searchField.font = .regularFont(with: .middle)
        searchField.textColor = .textColor
        placeholderLabel.textColor = .textColor
        searchField.backgroundColor = .white
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CitySearchBar:UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        endEditing(true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleSearchText), object: nil)
        self.perform(#selector(handleSearchText), with: nil, afterDelay: 0.5)
    }
    
    
}

extension CitySearchBar{
    @objc
    func handleSearchText(){
        guard let searchText = searchText,searchText.isValidCityName() else {return}
        citySearchBarDelegate?.getSearchedText(searchText)
    }
}
