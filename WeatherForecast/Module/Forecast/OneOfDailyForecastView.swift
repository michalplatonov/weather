//
//  OneOfDailyForecastView.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import UIKit
import SnapKit
import LBTATools

class OneOfDailyForecastView:UIView{
    
    var oneOfDailyForecast:OneDayOfDailyForecast?{
        didSet{
            guard let oneOfDailyForecast = oneOfDailyForecast,
                  let maxTemperature = oneOfDailyForecast.dailyForecasts.first?.temperature.maximum,
                  let minTemperature = oneOfDailyForecast.dailyForecasts.first?.temperature.minimum,
                  let dayIcon = oneOfDailyForecast.dailyForecasts.first?.day.icon,
                  let nightIcon = oneOfDailyForecast.dailyForecasts.first?.night.icon,
                  let dayPhrase = oneOfDailyForecast.dailyForecasts.first?.day.iconPhrase,
                  let nightPhrase = oneOfDailyForecast.dailyForecasts.first?.day.iconPhrase
            else { return }
            let maxUnit = maxTemperature.unit
            let minUnit = minTemperature.unit
            let minTemperatureColor = minTemperature.getTemperatureColor()
            let maxTemperatureColor = maxTemperature.getTemperatureColor()
            maxTodayTemperatureLabel.text = "\(Int(maxTemperature.value)) " + maxUnit
            minTodayTemperatureLabel.text = "\(Int(minTemperature.value)) " + minUnit
            maxTodayTemperatureLabel.textColor = maxTemperatureColor
            minTodayTemperatureLabel.textColor = minTemperatureColor
            dayTodayPhraseLabel.text = dayPhrase
            nightTodayPhraseLabel.text = nightPhrase
            dayWeatherImageView.image = UIImage(named: "\(dayIcon)")
            nightWeatherImageView.image = UIImage(named: "\(nightIcon)")
            self.setView()
        }
    }
    let todayTemperatureLabel = UILabel(text: "Pogoda dzisiaj", font: .boldFont(with: .large), textColor: .textColor, textAlignment: .center, numberOfLines: 0)
    let maxTodayTemperatureTitleLabel = UILabel(text: "Min.", font: .boldFont(with: .extraLarge), textColor: .textColor, textAlignment: .center, numberOfLines: 0)
    let minTodayTemperatureTitleLabel = UILabel(text: "Max.", font: .boldFont(with: .extraLarge), textColor: .textColor, textAlignment: .center, numberOfLines: 0)
    let maxTodayTemperatureLabel = UILabel(text: "", font: .boldFont(with: .large), textColor: .textColor, textAlignment: .center, numberOfLines: 0)
    let minTodayTemperatureLabel = UILabel(text: "", font: .boldFont(with: .large), textColor: .textColor, textAlignment: .center, numberOfLines: 0)
    let dayTodayPhraseLabel = UILabel(text: "", font: .boldFont(with: .middle), textColor: .textColor, textAlignment: .center, numberOfLines: 2)
    let nightTodayPhraseLabel = UILabel(text: "", font: .boldFont(with: .middle), textColor: .textColor, textAlignment: .center, numberOfLines: 2)
    let dayWeatherImageView = UIImageView(image: UIImage(data: Data()),contentMode: .scaleAspectFit)
    let nightWeatherImageView = UIImageView(image: UIImage(data: Data()),contentMode: .scaleAspectFit)
    let view = UIView(backgroundColor: .lightGray.withAlphaComponent(0.3))
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}


//MARK: UI

extension OneOfDailyForecastView{
    private func setView(){
        addSubview(view)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.snp.makeConstraints { make in
            make.top.equalTo(snp.top).offset(5)
            make.bottom.equalTo(snp.bottom).offset(-5)
            make.left.equalTo(snp.left).offset(5)
            make.right.equalTo(snp.right).offset(-5)
        }
        
        let maxStack = stack(maxTodayTemperatureTitleLabel,maxTodayTemperatureLabel)
        let minStack = stack(minTodayTemperatureTitleLabel,minTodayTemperatureLabel)
        let dayStack = stack(dayWeatherImageView,dayTodayPhraseLabel)
        let nightStack = stack(nightWeatherImageView,nightTodayPhraseLabel)
        
        
        let minMaxHStack = hstack(minStack,maxStack,spacing: 50,alignment: .center,distribution: .fillEqually)
        let dayNightHStack = hstack(dayStack,nightStack,spacing: 10,alignment: .center,distribution: .fillEqually)
        [maxStack,minStack,dayStack,nightStack,minMaxHStack,dayNightHStack].forEach{$0.translatesAutoresizingMaskIntoConstraints = false}
        
        let mainStack = stack(minMaxHStack,dayNightHStack,spacing: -40, alignment: .center,distribution: .fillEqually)
        [todayTemperatureLabel,mainStack].forEach{view.addSubview($0)}
        
        todayTemperatureLabel.snp.makeConstraints { make in
            make.top.equalTo(view.snp.top).offset(10)
            make.centerX.equalTo(view.snp.centerX)
        }
        mainStack.snp.makeConstraints { make in
            make.top.equalTo(todayTemperatureLabel.snp.bottom)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
}
