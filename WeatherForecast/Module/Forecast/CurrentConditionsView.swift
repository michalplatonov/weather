//
//  CurrentConditionsView.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import LBTATools
import SnapKit
import UIKit

class CurrentConditionsView:UIView{
    
    
    var currentConditions:CurrentConditions?{
        didSet{
            guard let currentConditions = currentConditions else {
                return
            }
            let temperature = Int(currentConditions.temperature.metric.value)
            let unit = currentConditions.temperature.metric.unit
            let color = currentConditions.temperature.metric.getTemperatureColor()
            temperatureLabel.text = "\(temperature) " + unit
            temperatureLabel.textColor = color
            weatherImageView.image = UIImage(named: "\(currentConditions.weatherIcon)")
            self.setView()
        }
    }
    var searchCityElement:SearchCityElement?{
        didSet{
            guard let searchCityElement = searchCityElement else {
                return
            }
            cityNameLabel.text = searchCityElement.localizedName
        }
    }
    let cityNameLabel = UILabel(text: "", font: .boldFont(with: .large), textColor: .textColor, textAlignment: .center, numberOfLines: 0)
    let weatherImageView = UIImageView(image: UIImage(data: Data()),contentMode: .scaleAspectFit)
    let temperatureLabel = UILabel(text: "", font: .regularFont(with: .extraLarge), textColor: .textColor, textAlignment: .center, numberOfLines: 0)
    let view = UIView(backgroundColor: .lightGray.withAlphaComponent(0.3))
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


//MARK: UI

extension CurrentConditionsView{
    private func setView(){
        addSubview(view)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.snp.makeConstraints { make in
            make.top.equalTo(snp.top).offset(5)
            make.bottom.equalTo(snp.bottom).offset(-5)
            make.left.equalTo(snp.left).offset(5)
            make.right.equalTo(snp.right).offset(-5)
        }
        
        [cityNameLabel,weatherImageView,temperatureLabel].forEach{view.addSubview($0)}
        weatherImageView.snp.makeConstraints { make in
            make.top.equalTo(view.snp.top).offset(10)
            make.centerX.equalTo(view.snp.centerX)
            make.height.equalTo(60)
        }
        cityNameLabel.snp.makeConstraints { make in
            make.top.equalTo(weatherImageView.snp.bottom)
            make.centerX.equalTo(view.snp.centerX)
        }
        temperatureLabel.snp.makeConstraints { make in
            make.top.equalTo(cityNameLabel.snp.bottom).offset(5)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
}
