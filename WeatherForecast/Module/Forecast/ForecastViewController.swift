//
//  ForecastViewController.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import UIKit
import LBTATools
import SnapKit

class ForecastViewController:BaseViewController{
    var searchCityElement:SearchCityElement
    var currentConditions:CurrentConditions?
    var oneOfDailyForecast:OneDayOfDailyForecast?
    let currentConditionView = CurrentConditionsView()
    let oneOfDailyForecastView = OneOfDailyForecastView()
    let group = DispatchGroup()
    let activityIndicator = UIActivityIndicatorView()
    init(searchCityElement:SearchCityElement){
        self.searchCityElement = searchCityElement
        super.init(nibName: nil, bundle: nil)
    }
    var error:Error?
    override func viewDidLoad() {
        super.viewDidLoad()
        setActivityIndicator()
        makeRequests()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: UI
extension ForecastViewController{
    func analizeRequestData(){
        activityIndicator.stopAnimating()
        print("i'm here")
        if let error = error{
            guard let error = error as? ApiError,let message = error.message else {
                self.showAlertViewController(where: error.localizedDescription)
                return
            }
            self.showAlertViewController(where: message)
        }
        else{
            self.setViews()
        }

    }
    func setViews(){
        [currentConditionView,oneOfDailyForecastView].forEach{view.addSubview($0)}
        
        currentConditionView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(10)
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left).offset(10)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right).offset(-10)
            make.height.equalTo(150)
        }
        oneOfDailyForecastView.snp.makeConstraints { make in
            make.top.equalTo(currentConditionView.snp.bottom).offset(10)
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left).offset(10)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right).offset(-10)
            make.height.equalTo(200)
        }
        
        currentConditionView.searchCityElement = searchCityElement
        currentConditionView.currentConditions = currentConditions
        oneOfDailyForecastView.oneOfDailyForecast = oneOfDailyForecast
    }
    func setActivityIndicator(){
        activityIndicator.color = .textColor
        view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(10)
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left).offset(10)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right).offset(-10)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-10)
        }
        activityIndicator.startAnimating()
    }
}

// MARK: Requests
extension ForecastViewController{
    func makeRequests(){
        group.enter()
        group.enter()
        getCurrentCondition { [weak self] in
            self?.group.leave()
        }
        getOneOfDailyForecast { [weak self] in
            self?.group.leave()
        }
        group.notify(queue: .main) {[weak self] in
            self?.analizeRequestData()
        }
    }
    func getCurrentCondition(completion: @escaping () -> Void){
        let queryItem = URLQueryItem(name: "language", value: "pl")
        let endpoint = EndPoint(path: Path.currentConditions.rawValue + searchCityElement.key, queryItems: [queryItem])
        let service = CurrentConditionsService<CurrentConditions>(endPoint: endpoint)
        service.getCurrentConditionList { [weak self] result in
            switch result{
            case .success(let currentConditionList):
                if currentConditionList.count > 0{
                    self?.currentConditions = currentConditionList.first
                }
                completion()
            case .failure(let error):
                self?.error = error
                completion()
            }
        }
    }
    func getOneOfDailyForecast(completion: @escaping () -> Void){
        let languageQuery = URLQueryItem(name: "language", value: "pl")
        let detailsQuery = URLQueryItem(name: "details", value: "true")
        let metricQuery = URLQueryItem(name: "metric", value: "true")
        let endpoint = EndPoint(path: Path.forecastsDailyOneDay.rawValue + searchCityElement.key, queryItems: [languageQuery,detailsQuery,metricQuery])
        let service = OneDayOfDailyForecastService<OneDayOfDailyForecast>(endPoint: endpoint)
        service.getOneDayOfDailyForecastObject { [weak self] result in
            switch result{
            case .success(let oneOfDailyForecast):
                self?.oneOfDailyForecast = oneOfDailyForecast
                completion()
            case .failure(let error):
                self?.error = error
                completion()
            }
        }
    }
}
