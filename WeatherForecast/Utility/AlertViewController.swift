//
//  AlertViewController.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation
import UIKit


extension UIViewController{
    func showAlertViewController(where description:String){
        DispatchQueue.main.async {[weak self] in
            let alertController = UIAlertController(title: "Error", message: description, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self?.present(alertController, animated: true)
        }
        
    }
}
