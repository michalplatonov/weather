//
//  Colors.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation
import UIKit


extension UIColor{
    convenience init(hex:String){
            var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString.remove(at: cString.startIndex)
            }
            
            var rgbValue:UInt64 = 0
            Scanner(string: cString).scanHexInt64(&rgbValue)
            
            self.init(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }
    static let lightGray = UIColor(hex: "D3D3D3")
    static let textColor = UIColor(hex: "000000")
    static let secondaryTextColor = UIColor(hex: "979797")
    static let lightBlue = UIColor(hex: "4FC3F7")
    static let lightRed = UIColor(hex: "E57373")
    static let navigationBarColor = UIColor(hex: "797EF6")
    
}
