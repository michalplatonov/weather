//
//  Fonts.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import UIKit

extension UIFont{
    enum FontSize:CGFloat{
        case small = 12
        case middle = 14
        case large = 16
        case extraLarge = 20
    }
    static private func findBetterSize(with font:FontSize)->CGFloat{
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let baseWidth: CGFloat = 375
        let fontSize = font.rawValue * (width / baseWidth)
        return fontSize
    }
    static func regularFont(with size:FontSize) -> UIFont{
        return UIFont.systemFont(ofSize: findBetterSize(with: size))
    }
    static func boldFont(with size:FontSize) -> UIFont{
        return UIFont.boldSystemFont(ofSize: findBetterSize(with: size))
    }
}
