//
//  BaseViewController.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation
import UIKit

class BaseViewController:UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setNavigationBar()
    }
    func setNavigationBar(){
        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .navigationBarColor
            
            appearance.titleTextAttributes = [.font: UIFont.regularFont(with: .extraLarge),
                                              .foregroundColor: UIColor.white]
            
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearance
        } else {
            
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.barTintColor = .navigationBarColor
            navigationController?.navigationBar.titleTextAttributes = [
                .font: UIFont.regularFont(with: .extraLarge),
                .foregroundColor: UIColor.white]
        }
    }
}
