//
//  UITableView+EmptyView.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import LBTATools
import UIKit

extension UITableView{
    func setEmptyTitle(_ title:String){
        let titleLabel = UILabel(text: title, font: .regularFont(with: .large), textColor: .textColor, textAlignment: .center, numberOfLines: 2)
        titleLabel.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.width)
        titleLabel.sizeToFit()
        self.backgroundView = titleLabel
    }
    func restore(){
        self.backgroundView = nil
    }
}
