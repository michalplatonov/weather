//
//  DataDecode.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation

extension Data {
    func decode<T:Decodable>(_ type: T.Type) throws -> T{
    return try JSONDecoder().decode(type, from: self)
  }
}
