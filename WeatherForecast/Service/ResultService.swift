//
//  ResultService.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation

enum NetworkingError: Error {
    case invalidURL
    case badResponse
    case noInternet
    case unexpectedStatus(ApiError)
}
struct ApiError:Error,Codable {
    var code:String
    var message:String? = nil
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case message = "Message"
    }
}

