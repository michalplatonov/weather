//
//  SearchCityService.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation

class SearchCityRequest<T:Codable>{
    private let urlSession = URLSession.urlSession
    var endPoint:EndPoint
    init(endPoint:EndPoint) {
        self.endPoint = endPoint
    }
    func getSearchedCityList(completion:@escaping (Result<[T],Error>) -> ()){
        guard let url = endPoint.url else {
            return completion(.failure(NetworkingError.invalidURL))
        }
        urlSession.dataTask(with: url) { data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse,let data = data else {
                completion(.failure(NetworkingError.badResponse))
                return
            }
            do{
                guard 200..<400 ~= httpResponse.statusCode else{
                    let apiError = try data.decode(ApiError.self)
                    completion(.failure(apiError))
                    return
                }
                let object = try data.decode([T].self)
                completion(.success(object))
            }catch let error{
                completion(.failure(error))
            }
            
            
            
        }.resume()
        
    }
    
}
