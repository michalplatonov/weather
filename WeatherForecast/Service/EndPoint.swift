//
//  EndPoint.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation

public enum Path:String{
    case locationCitySearch = "/locations/v1/cities/search"
    case currentConditions = "/currentconditions/v1/"
    case forecastsDailyOneDay = "/forecasts/v1/daily/1day/"
}

public struct EndPoint{
    private let path: String
    private var queryItems: [URLQueryItem]?
    private let apiKey = "okJ0EzV9QSK7P0ZvOmbUwLOmE8UaFQrw"
    init(path: String, queryItems: [URLQueryItem] = [URLQueryItem]()) {
        self.path = path
        self.queryItems = queryItems
        self.queryItems?.append(URLQueryItem(name: "apikey", value: apiKey))
    }
    var url:URL?{
        var components = URLComponents()
        components.scheme = "https"
        components.host = "dataservice.accuweather.com"
        components.path = path
        components.queryItems = queryItems
        return components.url
    }
    
}

