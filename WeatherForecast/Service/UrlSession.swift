//
//  UrlSession.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import UIKit



extension URLSession{

    static let urlSession: URLSession = {
        let urlSessionConfig = URLSessionConfiguration.default
        urlSessionConfig.timeoutIntervalForRequest = 30
        let session = URLSession(configuration: urlSessionConfig)
        return session
    }()
    
}

