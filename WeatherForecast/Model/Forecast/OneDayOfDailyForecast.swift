//
//  OneDayOfDailyForecast.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation

struct OneDayOfDailyForecast: Codable {
    let dailyForecasts: [DailyForecast]

    enum CodingKeys: String, CodingKey {
        case dailyForecasts = "DailyForecasts"
    }
    struct DailyForecast: Codable {
        let epochDate: Int
        let sun: Sun
        let temperature, realFeelTemperature: TemperatureArray
        let day, night: Time

        enum CodingKeys: String, CodingKey {
            case epochDate = "EpochDate"
            case sun = "Sun"
            case temperature = "Temperature"
            case realFeelTemperature = "RealFeelTemperature"
            case day = "Day"
            case night = "Night"
        }
        
        
    }
    struct Time: Codable {
        let icon: Int
        let iconPhrase: String
        enum CodingKeys: String, CodingKey {
            case icon = "Icon"
            case iconPhrase = "IconPhrase"
        }
    }
    struct TemperatureArray: Codable {
        let minimum, maximum: Metric
        enum CodingKeys: String, CodingKey {
            case minimum = "Minimum"
            case maximum = "Maximum"
        }
    }
    struct Sun: Codable {
        let epochRise: Int
        let epochSet: Int
        enum CodingKeys: String, CodingKey {
            case epochRise = "EpochRise"
            case epochSet = "EpochSet"
        }
    }
}



