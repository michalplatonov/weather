//
//  Metric.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import UIKit


struct Metric: Codable {
    let value: Double
    let unit: String

    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case unit = "Unit"
        
    }
    func getTemperatureColor() -> UIColor{
        switch value{
        case let value where value < 10:
            return .lightBlue
        case 10 ... 20:
            return .black
        case let value where value > 20:
            return .lightRed
        default:
            return .lightText
        }
    }
}

