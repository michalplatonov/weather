//
//  CurrentConditions.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation
import UIKit

struct CurrentConditions: Codable {
    struct Temperature: Codable {
        let metric: Metric
        enum CodingKeys: String, CodingKey {
            case metric = "Metric"
        }
        
    }
    
    let epochTime: Int
    let weatherIcon: Int
    let temperature: Temperature

    enum CodingKeys: String, CodingKey {
        case epochTime = "EpochTime"
        case weatherIcon = "WeatherIcon"
        case temperature = "Temperature"
    }
    
    
}
