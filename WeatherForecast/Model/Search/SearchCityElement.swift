//
//  SearchCity.swift
//  WeatherForecast
//
//  Created by Михаил Платонов on 05/12/2021.
//

import Foundation

struct SearchCityElement: Codable {
    
    let key: String
    let localizedName: String
    let country: Country

    enum CodingKeys: String, CodingKey {
        case key = "Key"
        case localizedName = "LocalizedName"
        case country = "Country"
    }
}

struct Country: Codable {
    let id, localizedName, englishName: String

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case localizedName = "LocalizedName"
        case englishName = "EnglishName"
    }
}
