//
//  CityRegexTest.swift
//  WeatherForecastTests
//
//  Created by Михаил Платонов on 05/12/2021.
//

import XCTest
@testable import WeatherForecast

class CityRegexTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCityName() throws {
//        let cityName = "NY"
//        let cityName = "San Francisco"
//        let cityName = "München"
//        let cityName = "København"
//        let cityName = "Saint-Tropez"
        let cityName = "St. Lucia"
        let cityNameCheck = cityName.isValidCityName()
        XCTAssertTrue(cityNameCheck,"\(cityName) is not valid")
    }
}
